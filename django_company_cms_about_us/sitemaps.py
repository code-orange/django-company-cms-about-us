from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class AboutUsStaticViewSitemap(Sitemap):
    def items(self):
        return ["about_us"]

    def location(self, item):
        return reverse(item)
