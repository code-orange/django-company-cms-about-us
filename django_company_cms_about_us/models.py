from datetime import datetime

from django.db import models
from parler.models import TranslatedFields, TranslatableModel


class AboutUsHistory(TranslatableModel):
    picture = models.ImageField(
        upload_to="django_company_cms_general.CmsFiles/bytes/filename/mimetype",
        blank=False,
        null=False,
    )

    date = models.DateField(default=datetime.now)

    translations = TranslatedFields(
        name=models.CharField(max_length=100),
        description=models.TextField(),
        translation_needed=models.BooleanField(default=True),
    )

    class Meta:
        db_table = "django_company_cms_about_us_history"
        ordering = ["-date"]
