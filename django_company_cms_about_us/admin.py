from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_company_cms_about_us").models.values():
    admin.site.register(model)
