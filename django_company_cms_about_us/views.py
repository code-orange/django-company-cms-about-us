from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_about_us.django_company_cms_about_us.models import (
    AboutUsHistory,
)
from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)


def home(request):
    template = loader.get_template("django_company_cms_about_us/home.html")

    template_opts = dict()

    company_name = CompanyCmsGeneral.objects.get(name="company_name")

    template_opts["content_title_main"] = company_name.content
    template_opts["content_title_sub"] = _("About Us")

    about_us_timeline = AboutUsHistory.objects.all()
    template_opts["about_us_timeline"] = about_us_timeline

    template_opts["about_us_01_header"] = CompanyCmsGeneral.objects.get(
        name="about_us_01_header"
    ).content

    template_opts["about_us_01_content"] = CompanyCmsGeneral.objects.get(
        name="about_us_01_content"
    ).content

    template_opts["about_us_02_header"] = CompanyCmsGeneral.objects.get(
        name="about_us_02_header"
    ).content

    template_opts["about_us_02_content"] = CompanyCmsGeneral.objects.get(
        name="about_us_02_content"
    ).content

    template_opts["about_us_03_header"] = CompanyCmsGeneral.objects.get(
        name="about_us_03_header"
    ).content

    template_opts["about_us_03_content"] = CompanyCmsGeneral.objects.get(
        name="about_us_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))
